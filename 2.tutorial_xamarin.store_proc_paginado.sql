use TutorialXamarin
GO

create procedure tutxam_seleccionarMunicipios(
	@RegistrosPorPagina int = 10,
	@PaginaActual int = 1,
	@TotalPaginas int = 0 out,
	@TotalRegistros int = 0 out
) as begin
	declare @Residuo float set @Residuo = 0
	declare @PrimerRegistro int set @PrimerRegistro = 0
	declare @UltimoRegistro int set @UltimoRegistro = 0

	-- 1: Contar todos los Registros
	select 
		@TotalRegistros = count(Id) 
	from
		TutorialXamarin..Municipios
	-- Where 
	--Si la consulta lleva más filtros además de las páginas es necesario aplicarlos aquí para que el conteo de registros sea correcto
		
	-- 2: Determinar Totakl Páginas
	set @TotalPaginas = @TotalRegistros / @RegistrosPorPagina
	set @Residuo = @TotalRegistros % @RegistrosPorPagina 

	--Calcular el rango de registros a mostrar en la página actual
	set @PrimerRegistro = (@PaginaActual * @RegistrosPorPagina) - (@RegistrosPorPagina - 1)
	set @UltimoRegistro = (@PaginaActual * @RegistrosPorPagina)

	if @Residuo > 0
	begin
		set @TotalPaginas = @TotalPaginas + 1
	end;
	
	with municipiosEnumerados as(
		select
			 ROW_NUMBER() OVER (ORDER BY Id) AS ROW_ID -- Enumerar cada registro 
			,Id
			,Nombre
		from
			TutorialXamarin..Municipios
		-- Where 
		--Si la consulta lleva más filtros además de las páginas es necesario aplicarlos aquí para que el conteo de registros sea correcto
	) select
		 Id
		,Nombre
	from municipiosEnumerados
	where ROW_ID between @PrimerRegistro and @UltimoRegistro
end
go